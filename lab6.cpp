/**************************
 *Anthony Alber
 *CPSC 1021 003 F20
 *aralber@clemson.edu
 *McMillan, Xu
 **************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

// Struct employee definition
typedef struct Employee {
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
} employee;

// Function prototypes (and definition for myrandom)
bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}

// Main function
int main(int argc, char const *argv[]) {
  // Random number generator seed
  srand(unsigned (time(0)));
  // Declare list of 10 employees
  employee list[10];
  // Iterate through list
  cout << endl;
  for (int i = 0; i < 10; ++i) {
    // Prompt for and read in employee data members
    cout << "[" << i + 1 << "] " << "Enter first name: ";
    cin >> list[i].firstName;
    cout << "[" << i + 1 << "] " << "Enter last name: ";
    cin >> list[i].lastName;
    cout << "[" << i + 1 << "] " << "Enter birth year: ";
    cin >> list[i].birthYear;
    cout << "[" << i + 1 << "] " << "Enter hourly wage: ";
    cin >> list[i].hourlyWage;
    cout << endl;
  }
  // Call random_shuffle (online resources say function object is a valid argument)
  random_shuffle(begin(list), end(list), myrandom);
  // Declare and initialize sublist of 5 of 10 employees
  employee list2[5] = {list[0], list[1], list[2], list[3], list[4]}; 
  // Sort the sublist by last name
  sort(begin(list2), end(list2), name_order);
  // List header
  cout << "------------------------" << endl;
  cout << "-----Sorted Sublist-----" << endl;
  cout << "------------------------" << endl;
  // Iterate through sublist
  for (auto i: list2) {
    // Print employee data members
    cout << setw(22 - i.firstName.size()) << i.lastName << ", " << i.firstName << endl;
    cout << setw(24) << i.birthYear << endl;
    cout << setw(24) << fixed << showpoint << setprecision(2) << i.hourlyWage << endl;
  }
  cout << endl;

  return 0;
}


/*
  name_order: compares order of lastName data members of two employee objects
  Parameters: two references of type const employee
  Return: bool
*/
bool name_order(const employee& lhs, const employee& rhs) {
  // Compare order of lastName data members
  if (lhs.lastName < rhs.lastName) {
    return true;
  } else {
    return false;
  }
}

